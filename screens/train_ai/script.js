let loader_stack = {};
let tags = {}


// Interface for warning message
function showWarning(text, type) {
    type = type || 'basic';
    const warningDOM = document.querySelector('.warning');

    warningDOM.className = 'warning';
    void warningDOM.offsetWidth;

    warningDOM.querySelector('.warning__text').innerText = text;

    switch (type) {
        case 'danger':
            warningDOM.classList.add('danger')
        case 'success':
            warningDOM.classList.add('success')
    }
    warningDOM.classList.add('show');
}

document.querySelector('.warning').onclick = () => {
    document.querySelector('.warning').className = 'warning';
}

// Interface for warning message
function showLoader(text) {
    const loaderDOM = document.querySelector('.loader');
    loaderDOM.querySelector('.loader__text').innerText = text;
    loaderDOM.className = 'loader';

    const loaderId = create_UUID();
    loader_stack[loaderId] = text;
    return loaderId
}

function hideLoader(loaderId) {

    if (Object.keys(loader_stack).includes(loaderId)) {
        delete loader_stack[loaderId]
    }

    if (Object.keys(loader_stack).length == 0) {
        document.querySelector('.loader').className = 'loader hidden';
    }
    else {
        document.querySelector('.loader').querySelector('.loader__text').innerText = loader_stack[Object.keys(loader_stack)[0]]
    }
}

// Capturing image from current camera
async function captureImage() {

    const videoBounding = videoPlayer.getBoundingClientRect();

    videoPlayer.className = 'hidden';
    const streamSettings = await videoPlayer.srcObject.getTracks()[0].getSettings();

    if (streamSettings.width) {
        videoPlayer.style.width = `${streamSettings.width}px`
    }
    if (streamSettings.height) {
        videoPlayer.style.height = `${streamSettings.height}px`
    }

    videoPlayer.style.width = `${streamSettings.width || videoBounding.width}`

    const canvas = document.createElement('canvas');
    canvas.width = videoBounding.width;
    canvas.height = videoBounding.height;
    if (streamSettings.width && streamSettings.height) {
        canvas.width = streamSettings.width;
        canvas.height = streamSettings.height;
        canvas.getContext("2d").drawImage(videoPlayer, 0, 0, streamSettings.width, streamSettings.height);
    }
    else {
        canvas.getContext("2d").drawImage(videoPlayer, 0, 0, videoBounding.width, videoBounding.height);
    }

    let base64Image = canvas.toDataURL("image/png");

    videoPlayer.style.width = 'auto'
    videoPlayer.style.height = 'auto'
    return base64Image;
}


// Displays available tags
async function showTagsList() {
    videoPlayer.className = 'hidden'
    const loaderId = showLoader("Fetching tags list");
    toggleButtons(true);

    const fetchedTags = await api_getLabels();

    if (fetchedTags) {
        let new_tags = {};
        fetchedTags.forEach(tag => {
            new_tags[tag] = Object.keys(tags).includes(tag) ? tags[tag] : true;
        })
        tags = new_tags;
    }

    document.querySelector('.tags_list').className = 'tags_list';
    document.querySelector('.tags_list').offsetWidth;
    document.querySelector('.tags_list').className = 'tags_list show';

    document.querySelector('ul.tags').querySelectorAll('*').forEach(node => {
        node.remove();
    })
    Object.keys(tags).forEach(tag => {
        const tagDOM = document.createElement('li');
        tagDOM.setAttribute('data-tagname', tag);

        const checkbox = document.createElement('input');
        checkbox.className = 'tag__checkbox';

        checkbox.setAttribute('type', 'checkbox');
        checkbox.checked = tags[tag];
        tagDOM.className = 'tag';

        tagDOM.innerHTML = `<span class="tag__title">${tag}</span>`

        tagDOM.appendChild(checkbox)

        document.querySelector('ul.tags').appendChild(tagDOM);

        checkbox.onclick = () => {
            if (Object.keys(tags).includes(tag)) {
                tags[tag] = !tags[tag];
            }
        }
    })

    videoPlayer.className = '';
    hideLoader(loaderId);
    toggleButtons(false);
}

// Toggles bottom buttons
function toggleButtons(disabled) {
    document.getElementById('capture').disabled = Boolean(disabled);
    document.getElementById('change-track').disabled = Boolean(disabled);
    document.getElementById('chouse-tags').disabled = Boolean(disabled);
}


// Hadler for capture button
let isCaptured = false;
async function captureImageEventHandler() {
    if (isCaptured) {
        document.getElementById('capture').innerText = 'Capture';
        document.getElementById('change-track').className = '_ui__button__default'
        document.getElementById('chouse-tags').className = '_ui__button__default'

        document.querySelector('.result_image').className = 'result_image hidden';
        videoPlayer.className = '';

        isCaptured = false;
    }
    else {
        isCaptured = true;
        document.getElementById('capture').innerText = 'Back';
        document.getElementById('change-track').className = '_ui__button__default hidden';
        document.getElementById('chouse-tags').className = '_ui__button__default hidden';

        const loaderId = showLoader("Sending image to AI server");
        toggleButtons(true);


        const image = await captureImage().then().catch();

        const req = await api_putImage(
            image,
            Object.keys(tags).filter((tag) => { return tags[tag] })
        ).catch(() => showWarning("Something went wrong", "danger"))

        if (req == 200) {
            showWarning("Success", "success")
        }
        else {
            showWarning("Something went wrong", "danger")
        }

        hideLoader(loaderId)
        toggleButtons(false);


        document.querySelector('.result_image').setAttribute('src', image);
        document.querySelector('.result_image').className = 'result_image';
    }
}


let devicesList = [];
let videoSourceIndex = 0;
let videoPlayer = document.getElementById("video-player");

// Helper for camera API
let MediaStreamHelper = {
    _stream: null,
    getDevices: function () {
        return navigator.mediaDevices.enumerateDevices();
    },
    requestStream: async function () {
        if (this._stream) {
            this._stream.getTracks().forEach(track => {
                track.stop();
            });
            this._stream.offsetWidth;
        }


        const videoSource = devicesList[videoSourceIndex];
        const constraints = {
            video: {
                deviceId: videoSource ? { exact: videoSource } : undefined
            },
            audio: false
        };

        return navigator.mediaDevices.getUserMedia(constraints);
    }
};


// Requesting for campera permission and getting list of available cameras
MediaStreamHelper.requestStream().then(function (stream) {
    MediaStreamHelper.getDevices().then((devices) => {
        devices.forEach((device) => {
            if (device.kind === "videoinput") {
                devicesList.push(device.deviceId);
            }
        });

        videoSourceIndex = devicesList.length - 1;
    }).catch(function (e) { }).finally(async () => {
        videoPlayer.className = 'hidden';
        const loaderId = showLoader("Initializing camera");
        toggleButtons(true);
        videoPlayer.onplay = () => {
            hideLoader(loaderId);
            toggleButtons(false);
            videoPlayer.className = '';
            videoPlayer.onplay = null;
        }
        await changeCamera();

    });
}).catch(function (err) {
    console.error(err);
});


// Change camera function
async function changeCamera() {
    await MediaStreamHelper.requestStream().then(async function (stream) {
        await videoPlayer.pause();
        videoPlayer.removeAttribute('srcObject');
        videoSourceIndex = (videoSourceIndex + 1) % devicesList.length;

        MediaStreamHelper._stream = stream;
        videoPlayer.srcObject = stream;
        await videoPlayer.play();
    }).then(() => {}).catch(error => {
        console.log(error)
        // changeCamera();
    });

}

// Main
async function app() {
    toggleButtons(true)

    document.getElementById('capture').onclick = async () => {
        await captureImageEventHandler();
    }

    document.getElementById('change-track').onclick = async () => {
        toggleButtons(true);
        let loaderId = showLoader("Changing camera");
        videoPlayer.className = 'hidden';
        await changeCamera();
        await videoPlayer.play();
        toggleButtons(false);
        videoPlayer.className = '';
        hideLoader(loaderId);
    }

    document.getElementById('chouse-tags').addEventListener('click', async () => {
        await showTagsList();
    })

    document.getElementById('close-tags-choice').addEventListener('click', () => {
        document.querySelector('.tags_list').className = 'tags_list';
        setTimeout(() => {
            document.querySelector('.tags_list').className = 'tags_list hidden';
        }, 300)
    })

    await showTagsList();
}

document.querySelector('video').setAttribute("playsinline", true);
app();