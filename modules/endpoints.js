async function api_getLabels() {
    let tags = null;

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    await fetch("https://vast-peak-10418.herokuapp.com/getLabels?engine=Azure&key=1", requestOptions)
        .then(response => response.text())
        .then(result => {
            const reponseData = JSON.parse(result);
            if (reponseData) {
                if (reponseData.labels) {
                    tags = reponseData.labels;
                }
            }
        })
        .catch(error => console.log('error', error));
    
    return tags
}

async function api_detectLabels(imageData) {
    var formdata = new FormData();
    formdata.append("engine", "azure");
    formdata.append("key", "1");
    formdata.append("b64image", imageData);

    var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
    };

    let data = null;

    await fetch("https://vast-peak-10418.herokuapp.com/detectLabels", requestOptions)
        .then(response => response.text())
        .then(result => {
            data = JSON.parse(result);
        })
        .catch((message, source, lineno, colno, error) => {
            showWarning("Something went wrong")
        });

    return data
}

async function api_putImage(imageData, tags) {
    var formdata = new FormData();
    formdata.append("key", "1");
    formdata.append("engine", "azure");
    formdata.append("image", imageData);
    formdata.append("tags", tags.join(';'));

    var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
    };

    let status;

    await fetch("https://vast-peak-10418.herokuapp.com/putTrainImage", requestOptions)
        .then(response => {status = response.status;response.text();})
        .then(result => {})
        .catch(error => console.log('error', error));
    
    return status
}

function blobToBase64(blob) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });

}

